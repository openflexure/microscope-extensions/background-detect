 # Background detect

An extension that will detect when the microscope is looking at an empty field of view.  This is useful if you are scanning a sample with empty areas, as it stops it from losing focus.

## Installing

This is an extension for the OpenFlexure microscope server.

For now, you should clone this repository, and symlink the `background_detect` folder into your plugins folder, usually `/var/openflexure/extensions/microscope_extensions/`.  If you are running the default OpenFlexure Raspbian image, that can be accomplished with the following commands:

```console
cd
git clone https://gitlab.com/openflexure/microscope-extensions/background-detect.git
sudo ln -s `pwd`/background-detect/background_detect /var/openflexure/extensions/microscope_extensions/
sudo ofm restart
```

## Using

A tab should appear in the microscope GUI allowing you to set the background parameters.  This is very clunky - you will need to scroll vertically to see some of the controls, and this isn't always obvious.

Adding this in to the scan routine is a work in progress.

import json
import logging
import re
import urllib.request
from time import sleep, time
import PIL
import io
import cv2
from scipy.stats import norm


import numpy as np
from flask import current_app as app
from labthings import current_action, fields, find_component
from labthings import find_component, find_extension
from labthings.extensions import BaseExtension
from labthings.find import current_labthing, registered_extensions
from labthings.utilities import path_relative_to
from openflexure_microscope.api.utilities.gui import build_gui
from openflexure_microscope.api.default_extensions.autofocus import AutofocusExtension, find_microscope
from .labthings_extension_decorators import extension_action, extension_property


def _gui_description(tolerance, threshold, status_field):
    return {
        "icon": "format_color_fill",
        "forms": [
            {
                "name": "Background value",
                "route": "/set_background_from_current_image",
                "submitLabel": "Set Background",
                "isTask": True,
                "schema": [
                    {
                        "fieldType": "htmlBlock",
                        "name": "status_display",
                        "label": "current status",
                        "content": (
                            "Position the microscope so that the image is "
                            "entirely background, then click \"set background\" below."
                        )
                    }
                ]
            },
            {
                "name": "detection_settings",
                "route": "/set_tolerance_and_threshold",
                "submitLabel": "Set",
                "schema": [
                    {
                        "fieldType": "numberInput",
                        "name": "tolerance",
                        "label": "Tolerance (standard deviations)",
                        "default": tolerance,
                        "min": 0,
                        "max": 20,
                        "step": 0.1,
                    },
                    {
                        "fieldType": "numberInput",
                        "name": "threshold_percentage",
                        "label": "Threshold to declare an image background (best to use >= 95%)",
                        "default": threshold*100,
                        "min": 0,
                        "max": 100,
                        "step": 0.1,
                    },
                ]
            },
            {
                "name": "status",
                "route": "/status",
                "submitLabel": "",
                "schema": [
                    {
                        "fieldType": "htmlBlock",
                        "name": "status_display",
                        "label": "current status",
                        "content": status_field
                    }
                ]
            },
        ]
        }

# tests whether each pixel in image is within multiple stdevs (stats_list[1]) of the mean (stats_list[0]) for all three channels
# returns a mask with True for pixels within that range (similar to stats list) and False otherwise

def check_pixels_against_distribution(image, channel_mean, channel_sd, tolerance):
    """Evaluate the pixels in an image against a gaussian distribution.

    Each pixel is compared against the calculated Gaussian distribution of the
    background pixels.  If it is more than `tolerance` standard deviations from
    the mean, it is classed as background.  

    The means and SDs should be lists or arrays of length 3, corresponding to the
    L, U, and V channels.
    
    The return value is a 2D binary image.
    """
    # ensure mean/sd are numpy arrays, and broadcast them over XY
    mean = np.array(channel_mean, dtype=float)[np.newaxis, np.newaxis, :]
    sd = np.array(channel_sd, dtype=float)[np.newaxis, np.newaxis, :]
    return np.all(
        np.abs(image - mean) < sd * float(tolerance),
        axis = 2
    )

# Create the extension class
class BackgroundDetectExtension(BaseExtension):
    _commands = {}

    def __init__(self):
        super().__init__(
            "org.openflexure.background-detect",
            version="0.0.1",
            static_folder=path_relative_to(__file__, "static"),
        )

        # This should be replaced with a labthings import in due course
        AutofocusExtension.add_decorated_method_views(self)
        #def gui_func():
        #    return {"icon": "code", "frame": {"href": self.static_file_url("")}}
        #
        #self.add_meta("gui", build_gui(gui_func, self))
        self.add_meta("gui", build_gui(self.gui, self))
        #self.add_meta(
        #    "gui", 
        #    build_gui(
        #        {
        #            "icon": "format_color_fill", 
        #            "frame": {"href": self.static_file_url("")}
        #        }, 
        #        self
        #    )
        #)

    tolerance = 5
    threshold = 0.97
    background_mean = None
    background_sd = None

    @extension_property(
        schema={
            "tolerance": fields.Float(description="The number of standard deviations from the mean within which pixels are counted as being in the background."),
            "threshold": fields.Float(description="The fraction of pixels (i.e. a number between 0 and 1, usually quite close to 1) that must count as background before an image counts as background"),
            "background_mean": fields.List(fields.Float(), description="L, U, V components of the mean pixel value in the background."),
            "background_sd": fields.List(fields.Float(), description="Standard deviation of the backround pixel values, in L, U, V colourspace."),
        }
    )
    def parameters(self):
        """A summary of the parameters used to identify whether an image contains any content, or only background pixels."""
        return {k: getattr(self, k) for k in ("tolerance", "threshold", "background_mean", "background_sd")}

    def gui(self):
        """Return a GUI description for the web app to build our interface"""
        try:
            current_classification = self.grab_and_classify_image()
            status_field = (
                "<div>\n"
                f"Current image is {current_classification['background_percentage']}% background\n<br />"
                f"Current threshold is {self.threshold * 100}%\n<br />"
                f"The current image is therefore classified as: {current_classification['classification']}"
                "</div>"
            )
        except ValueError:
            status_field = "<div>Background parameters have not yet been set</div>"
        return _gui_description(self.tolerance, self.threshold, status_field)

    def grab_image(self):
        """Grab an image and return it as a numpy array"""
        m = find_microscope()
        jpeg: bytes = m.camera.get_frame()
        return np.array(PIL.Image.open(io.BytesIO(jpeg)))

    @extension_action(args={})
    def set_background_from_current_image(self):
        """Grab an image, and use it to update the background (mean and SD)."""
        image = self.grab_image()
        background_LUV = cv2.cvtColor(image, cv2.COLOR_RGB2LUV)
        channels = np.array(
            [np.asarray((background_LUV[..., i]).flatten()) for i in range(3)]
        ).T

        # we get the mean and standard deviation of values in each channel
        mu, std = np.apply_along_axis(norm.fit, 0, channels)

        self.background_mean = mu
        self.background_sd = std

        return {"mean": mu, "standard_deviation": std}

    @extension_action(
        args={
            "tolerance": fields.Number(
                description="The tolerance to use when classifying pixels as background or foreground, in standard deviations."
            ),
            "threshold_percentage": fields.Number(
                description="The percentage of pixels that must be classed as background before an image is classed as background."
            )
        }
    )
    def set_tolerance_and_threshold(self, tolerance, threshold_percentage):
        self.tolerance = tolerance
        self.threshold = threshold_percentage/100

    @extension_action()
    def grab_and_classify_image(self):
        """Grab an image and classify it as background or foreground"""
        if not (
            self.background_mean is not None
            and self.background_sd is not None
            and np.isfinite(self.tolerance)
        ):
            raise ValueError("The background detect plugin cannot classify images: the background value or tolerance is not set.")
        image = self.grab_image()
        luv = cv2.cvtColor(image, cv2.COLOR_RGB2LUV)
        mask = check_pixels_against_distribution(luv, self.background_mean, self.background_sd, self.tolerance)
        coverage = np.count_nonzero(mask)/mask.size
        return {
            "background_coverage": coverage,
            "background_percentage": coverage * 100,
            "classification": "background" if coverage > self.threshold else "foreground"
        }

LABTHINGS_EXTENSIONS = (BackgroundDetectExtension,)